"""
@copyright: (C) 2008, Thomas Leonard
@see: U{http://roscidus.com}
"""
import os, sys

import _gio as gio
import gobject

builtin_commands = {}

def builtin(fn):
	builtin_commands[fn.__name__.replace('_', ':', 1)] = fn
	return fn

@builtin
def rox_trash(shell, args):
	for item in args:
		shell.cwd.file.resolve_relative_path(item).trash()
	l = len(args)
	if l == 1:
		return "Sent '%s' to trash" % args[0]
	else:
		return "Sent %d items to trash" % l

@builtin
def cd(shell, args):
	assert len(args) == 1, "Multiple selection not allowed for cd"
	shell.set_cwd(shell.cwd.file.resolve_relative_path(args[0]))

@builtin
def rox_open(shell, args):
	# Open a single item
	assert len(args) == 1, "Multiple selection not yet supported: " + str(args)
	shell.open_item(shell.cwd.file.resolve_relative_path(args[0]))
