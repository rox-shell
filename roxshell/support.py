"""
@copyright: (C) 2008, Thomas Leonard
@see: U{http://roscidus.com}
"""

import gobject
from logging import debug
import os
from zeroinstall.support import tasks		# tmp

def wait_for_blocker(blocker):
	"""Run a recursive mainloop until blocker is triggered.
	@param blocker: event to wait on
	@type blocker: L{tasks.Blocker}"""
	if not blocker.happened:
		loop = gobject.MainLoop(gobject.main_context_default())
		def quitter():
			yield blocker
			loop.quit()
		quit = tasks.Task(quitter(), "quitter")

		debug("Entering mainloop, waiting for %s", blocker)
		loop.run()

		assert blocker.happened, "Someone quit the main loop!"

	tasks.check(blocker)

def find_in_path(cmd):
	if os.path.isabs(cmd):
		return cmd
	for x in os.environ['PATH'].split(':'):
		full = os.path.join(x, cmd)
		if os.path.exists(full):
			return full
	return None

def split_expanded_path(value):
	"""Like os.path.split(os.path.expanduser(value))"""
	if value.startswith('~'):
		value = os.path.expanduser(value)
		if value.startswith('~') and '/' not in value:
			value = os.path.join(os.path.dirname(os.path.expanduser('~')), value[1:])
	return os.path.split(value)
