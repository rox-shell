"""
@copyright: (C) 2008, Thomas Leonard
@see: U{http://roscidus.com}
"""
import pygtk; pygtk.require('2.0')	# Needed to find gio!

from optparse import OptionParser
import logging

import support, app

def main(command_args):
	parser = OptionParser(usage="usage: %prog [options] [dir]...")
	parser.add_option("-v", "--verbose", help="more verbose output", action='count')
	parser.add_option("-V", "--version", help="display version information", action='store_true')

	(options, args) = parser.parse_args(command_args)

	if options.verbose:
		logger = logging.getLogger()
		if options.verbose == 1:
			logger.setLevel(logging.INFO)
		else:
			logger.setLevel(logging.DEBUG)

	support.wait_for_blocker(app.ApplicationController(args or ['.']).run())
