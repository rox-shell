"""
@copyright: (C) 2008, Thomas Leonard
@see: U{http://roscidus.com}
"""
from zeroinstall.support import tasks		# tmp

import _gio as gio

import shell

class ApplicationView:
	def __init__(self, app):
		self.app = app

	@tasks.async
	def run(self):
		view_tasks = []
		for item in self.app.items:
			cwd = gio.file_new_for_commandline_arg(item)
			view_tasks.append(shell.ShellView(cwd).run())

		while view_tasks:
			yield view_tasks
			tasks.check(view_tasks)
			view_tasks = [v for v in view_tasks if not v.happened]
	
class ApplicationController:
	def __init__(self, items):
		self.items = items
	
	def run(self):
		view = ApplicationView(self)
		return view.run()
