"""
@copyright: (C) 2008, Thomas Leonard
@see: U{http://roscidus.com}
"""
import os, sys, fnmatch
from zeroinstall.support import tasks		# tmp

import _gio as gio
import gobject
import gtk
import pango
from gtk import keysyms
import vte
import support

import commands
import directory
import line

DirModel = directory.DirModel

RETURN_KEYS = (keysyms.Return, keysyms.KP_Enter, keysyms.ISO_Enter)

FILER_PAGE = 0
TERMINAL_PAGE = 1

WRAP_WIDTH = 20		# Wrap width in chars (approx)

class ShellView:
	terminal = None
	user_seen_terminal_contents = False
	warning_timeout = None
	cwd = None
	view_dir = None

	def __init__(self, cwd_file):
		builder = gtk.Builder()
		builder.add_from_file(os.path.join(os.path.dirname(__file__), "ui.xml"))
		self.window = builder.get_object('directory')
		self.notebook = builder.get_object('notebook')

		cd_parent = builder.get_object('cd-parent')
		cd_parent.connect('activate', lambda a: self.cd_parent())

		cd_home = builder.get_object('cd-home')
		cd_home.connect('activate', lambda a: self.cd_home())

		trash = builder.get_object('trash')
		trash.connect('activate', lambda a: self.trash())

		# Must show window before adding icons, or we randomly get:
		# The error was 'BadAlloc (insufficient resources for operation)'
		self.window.show()

		self.window_destroyed = tasks.Blocker('Window destroyed')
		self.window.connect('destroy', lambda w: self.window_destroyed.trigger())

		self.window.connect('key-press-event', self.key_press_event)
	
		self.iv = builder.get_object('iconview')
		self.iv.set_text_column(0)
		self.iv.set_pixbuf_column(1)
		self.iv.set_selection_mode(gtk.SELECTION_MULTIPLE)

		text = self.iv.get_cells()[0]
		self.iv.set_attributes(text, text = DirModel.NAME, foreground = DirModel.COLOUR)
		pango_context = self.iv.get_pango_context()
		font_metrics = pango_context.get_metrics(self.iv.style.font_desc, pango_context.get_language())
		text.set_property('wrap-width', WRAP_WIDTH * font_metrics.get_approximate_char_width() / pango.SCALE)

		ui = builder.get_object('uimanager')

		accelgroup = ui.get_accel_group()
		self.window.add_accel_group(accelgroup)

		popup = ui.get_widget('ui/main-popup')

		self.iv.connect('item-activated', self.item_activated)
		def iv_button_press(widget, bev):
			if bev.type == gtk.gdk.BUTTON_PRESS and bev.button == 3:
				selected = self.iv.get_selected_items()
				pointer_path = self.iv.get_path_at_pos(int(bev.x), int(bev.y))
				if pointer_path and pointer_path not in selected:
					if pointer_path:
						self.iv.unselect_all()
						self.iv.select_path(pointer_path)
						self.iv.set_cursor(pointer_path)

				popup.popup(None, None, None, bev.button, bev.time)
		self.iv.connect('button-press-event', iv_button_press)

		command_area = builder.get_object('command')
		self.command_argv = line.ArgvView(command_area)

		self.status_msg = builder.get_object('status_msg')

		self.set_cwd(cwd_file)
		self.reset()

		self.window.show_all()
	
	def iter_contents(self):
		m = self.iv.get_model()
		i = m.get_iter_root()
		while i:
			yield i, m[i]
			i = m.iter_next(i)
	
	def warning(self, msg):
		def hide_warning():
			self.status_msg.set_text('')
			return False
		if self.warning_timeout is not None:
			gobject.source_remove(self.warning_timeout)
		self.status_msg.set_text(msg)
		self.warning_timeout = gobject.timeout_add(2000, hide_warning)
	
	def show_terminal(self):
		# Actually, don't show it until we get some output...
		if not self.terminal:
			def terminal_contents_changed(vte):
				if self.notebook.get_current_page() == FILER_PAGE:
					self.notebook.set_current_page(TERMINAL_PAGE)
				self.user_seen_terminal_contents = False

			def terminal_child_exited():
				if self.user_seen_terminal_contents:
					self.notebook.set_current_page(FILER_PAGE)
				else:
					self.terminal.feed('\r\nProcess complete. Press Return to return to filer view.\r\n')
					self.waiting_for_return = True
				return False

			self.terminal = vte.Terminal()
			self.terminal.connect('contents-changed', terminal_contents_changed)
			self.terminal.connect('child-exited', lambda vte: gobject.timeout_add(100, terminal_child_exited))

			# Should be configurable.
			# Hint:
			# cp /usr/share/fonts/X11/misc/9x15B-ISO8859-1.pcf.gz ~/.fonts/
			self.terminal.set_font(pango.FontDescription("Fixed 12"))

			self.terminal.show()

			self.notebook.add(self.terminal)
		self.waiting_for_return = False
	
	def reset(self):
		self.view_cwd()
		self.command_argv.set_args([line.CommandArgument(self), line.Argument(self)])
		if self.notebook.get_current_page() == FILER_PAGE:
			self.iv.grab_focus()
		self.iv.unselect_all()
	
	def key_press_event(self, window, kev):
		#for x in dir(keysyms):
		#	if getattr(keysyms, x) == kev.keyval:
		#		print x

		if self.terminal and self.terminal.flags() & gtk.HAS_FOCUS:
			if kev.keyval in RETURN_KEYS and self.waiting_for_return:
				self.notebook.set_current_page(FILER_PAGE)
				return True
			self.user_seen_terminal_contents = True
			return False

		if kev.keyval == keysyms.space:
			if self.command_argv.space():
				return True

		if kev.keyval == keysyms.Tab:
			if self.command_argv.tab():
				return True

		if kev.keyval == keysyms.Up:
			if self.command_argv.updown(-1):
				return True

		if kev.keyval == keysyms.Down:
			if self.command_argv.updown(1):
				return True

		if kev.keyval == keysyms.Escape:
			self.reset()
			return True
		elif kev.keyval in RETURN_KEYS:
			self.execute_command()
			return True

		# Are we ready for special characters?
		if self.command_argv.active_entry and self.command_argv.active_entry.flags() & gtk.HAS_FOCUS:
			accept_special = True		# TODO: check cursor is at end
		else:
			accept_special = True

		if accept_special:
			if kev.keyval == keysyms.comma:
				self.command_argv.activate(self.command_argv.args[0])
				return True
			elif kev.keyval == keysyms.semicolon and len(self.command_argv.args) == 2:
				self.command_argv.set_args([line.CommandArgument(self)])
				self.command_argv.widgets[0].grab_focus()
				return True

		if self.iv.flags() & gtk.HAS_FOCUS:
			if self.iv.event(kev):
				# Handled by IconView (e.g. cursor motion)
				return True
			elif kev.keyval == keysyms.BackSpace:
				self.cd_parent()
			else:
				if not self.command_argv.key_press_event(kev):
					self.iv.grab_focus()	# Restore focus to IconView
					return False
			return True

	def run_in_terminal(self, argv):
		cmd = support.find_in_path(argv[0])
		if not cmd:
			raise Warning("Command '%s' not found in $PATH" % argv[0])
		if not os.path.exists(cmd):
			raise Warning("Command '%s' does not exist!" % argv[0])

		self.show_terminal()
		self.user_seen_terminal_contents = True
		self.terminal.fork_command(cmd, argv, None, self.cwd.file.get_path(), False, False, False)
	
	def execute_command(self, override_command = None):
		try:
			self.command_argv.finish_edit()
			args = self.command_argv.args
			if override_command:
				override = line.CommandArgument(self)
				override.command = override_command
				args = [override] + args[1:]
			self.run_command(args)
		except Warning, ex:
			self.warning(str(ex))
		else:
			self.reset()

	def view_cwd(self):
		"""Make the IconView show the cwd."""
		if self.view_dir != self.cwd:
			self.set_view_dir(self.cwd.file)
	
	def set_view_dir(self, dir_file):
		if self.view_dir:
			self.view_dir.del_ref(self)
			self.view_dir = None
		self.view_dir = directory.get_dir_model(dir_file)
		self.view_dir.add_ref(self)

		self.window.set_title(self.view_dir.file.get_uri())

		# This segfaults. See GTK bug #523724.
		#tree_model = gtk.TreeModelSort(self.view_dir.model)
		#tree_model.set_sort_column_id(DirModel.SORT, gtk.SORT_ASCENDING)
		tree_model = self.view_dir.model

		self.iv.set_model(tree_model)
		if tree_model.get_iter_root():
			self.iv.set_cursor((0,))

		if self.view_dir.error:
			self.warning(str(self.view_dir.error))
	
	def set_cwd(self, cwd_file):
		if self.cwd:
			self.cwd.del_ref(self)
			self.cwd = None
		self.cwd = directory.get_dir_model(cwd_file)
		self.cwd.add_ref(self)
		self.view_cwd()
		self.reset()

	@tasks.async
	def run(self):
		self.iv.grab_focus()
		while True:
			blockers = [self.window_destroyed]
			yield blockers
			tasks.check(blockers)
			if self.window_destroyed.happened:
				break

	def get_iter(self, name):
		for i, row in self.iter_contents():
			if row[DirModel.NAME] == name:
				return i
		raise Exception("File '%s' not found!" % name)
	
	def get_cursor_path(self):
		return (self.iv.get_cursor() or (None, None))[0]

	def item_activated(self, iv, path):
		# Open a single item
		tm = iv.get_model()
		row = tm[tm.get_iter(path)]
		name = row[DirModel.NAME]
		item_info = row[DirModel.INFO]

		child = self.view_dir.file.get_child(name)

		self.reset()
		self.open_item(child)

	def open_item(self, item_file):
		item_info = item_file.query_info('standard::*', 0)
		if item_info.get_file_type() == gio.FILE_TYPE_DIRECTORY:
			self.set_cwd(item_file)
		else:
			self.run_in_terminal(['gvim', item_file.get_path()])
	
	def trash(self):
		self.execute_command('rox:trash')

	def cd_home(self):
		self.set_cwd(gio.file_new_for_path(os.path.expanduser('~')))

	def cd_parent(self):
		leaf = self.cwd.file.get_basename()
		parent = self.cwd.file.get_parent()
		if parent:
			self.set_cwd(parent)
			for i, row in self.iter_contents():
				if row[DirModel.NAME] == leaf:
					path = self.iv.get_model().get_path(i)
					self.iv.select_path(path)
					self.iv.set_cursor(path)
					break

	def run_command(self, args):
		argv = []
		for a in args:
			argv += a.expand_to_argv()
		if not argv[0]:
			argv[0] = 'rox:open'

		builtin = commands.builtin_commands.get(argv[0], None)
		if builtin:
			msg = builtin(self, argv[1:])
			if msg:
				self.warning(msg)	# Not really a warning, just info
		else:
			self.run_in_terminal(argv)

