"""
@copyright: (C) 2008, Thomas Leonard
@see: U{http://roscidus.com}
"""
from __future__ import with_statement
import gobject, stat
import _gio as gio
import gtk
import weakref
from logging import warn

gtk_theme = gtk.icon_theme_get_default()

icon_size = 48

def get_themed_icon(name, icon_size):
	try:
		return gtk_theme.load_icon('text-x-generic', icon_size, 0)
	except gobject.GError:
		return None

icon_text_plain = get_themed_icon('text-x-generic', icon_size)
icon_dir = get_themed_icon('folder', icon_size)

def sort_key(name, info):
	t = 9 - int(info.get_file_type())
	return str(t) + name.lower()

COLOUR_EXEC   = '#008000'
COLOUR_NORMAL = '#000000'
COLOUR_DIR    = '#0000a0'
COLOUR_ERROR  = '#800000'

QUERY = 'standard::*,unix::mode,thumbnail::path'

def is_hidden(name):
	return name.startswith('.')

class ErrorInfo:
	def __init__(self, name):
		self.name = name

	def get_name(self):
		return self.name

	def get_file_type(self):
		return gio.FILE_TYPE_UNKNOWN

def make_error_row(name, ex):
	info = ErrorInfo(name)
	return [name, None, info, sort_key(name, info), COLOUR_ERROR]

def make_row(info):
	name = info.get_name()
	if is_hidden(name):
		return None

	pixbuf = None
	thumbnail = info.get_attribute_byte_string('thumbnail::path')
	if thumbnail:
		try:
			loader = gtk.gdk.PixbufLoader('png')
			try:
				with open(thumbnail) as stream:
					loader.write(stream.read())
			finally:
				loader.close()
			pixbuf = loader.get_pixbuf()
			assert pixbuf, "Failed to load PNG thumbnail"
		except Exception, ex:
			warn("Failed to load cached PNG thumbnail icon: %s", ex)
	else:
		icon = info.get_icon()
		if icon:
			gtkicon_info = gtk_theme.choose_icon(icon.get_names(), icon_size, 0)
			if gtkicon_info:
				gtkicon_info.get_filename()
				pixbuf = gtkicon_info.load_icon()
	mode = info.get_attribute_uint32('unix::mode')
	if stat.S_ISDIR(mode):
		colour = COLOUR_DIR
	elif mode & 0111:
		colour = COLOUR_EXEC
	else:
		colour = COLOUR_NORMAL
	return [name, pixbuf, info, sort_key(name, info), colour]

class DirModel:
	NAME = 0
	PIXBUF = 1
	INFO = 2
	SORT = 3		# Type-then-name
	COLOUR = 4

	model = None
	monitor = None

	def __init__(self, file):
		assert isinstance(file, gio.File), file
		self.file = file
		self.users = []
	
	def __del__(self):
		assert not self.users, "DirModel garbage collected while still monitoring! Monitored by " + str(self.users)
	
	def add_ref(self, user):
		user = id(user)

		if not self.users:
			# First user...
			assert not self.monitor
			assert not self.model
			self.model = gtk.ListStore(str, gtk.gdk.Pixbuf, object, str, str)

			# Should really be the view that sorts, but see GTK bug #523724
			self.model.set_sort_column_id(DirModel.SORT, gtk.SORT_ASCENDING)

			self.monitor = self.file.monitor_directory(0)
			weakself = weakref.ref(self)
			self.monitor.connect('changed', lambda *args: weakself().contents_changed(*args))
			self.build_contents()
			
		self.users.append(user)
	
	def del_ref(self, user):
		user = id(user)
		self.users.remove(user)

		if not self.users:
			# Last user...
			self.model.clear()
			self.model = None
			self.monitor.cancel()
			self.monitor = None
	
	def contents_changed(self, monitor, this_file, other_file, event_type):
		name = this_file.get_basename()
		if is_hidden(name):
			return
		#print "build", monitor, this_file.get_basename(), other_file, event_type
		if event_type == gio.FILE_MONITOR_EVENT_CREATED:
			#print "Create", name
			i = self.model.append(None)
		elif event_type in (gio.FILE_MONITOR_EVENT_DELETED, gio.FILE_MONITOR_EVENT_CHANGED, gio.FILE_MONITOR_EVENT_ATTRIBUTE_CHANGED):
			i = self.model.get_iter_root()
			while i:
				if self.model[i][DirModel.NAME] == name:
					break
				i = self.model.iter_next(i)
			else:
				#print "Note: deleted unknown file", name
				self.build_contents()
				return
		elif event_type == gio.FILE_MONITOR_EVENT_CHANGES_DONE_HINT:
			return
		else:
			print "Unknown event type", event_type, name
			self.build_contents()

		if event_type in (gio.FILE_MONITOR_EVENT_CREATED, gio.FILE_MONITOR_EVENT_CHANGED, gio.FILE_MONITOR_EVENT_ATTRIBUTE_CHANGED):
			#print "Update", name
			try:
				info = this_file.query_info(QUERY, 0)
			except gobject.GError, ex:
				self.model[i] = make_error_row(name, ex)
			else:
				self.model[i] = make_row(info)
		elif event_type == gio.FILE_MONITOR_EVENT_DELETED:
			#print "Remove", name
			self.model.remove(i)
		else:
			assert False, "Unreachable! " + str(event_type)

	def build_contents(self):
		m = self.model
		name_to_iter = {}

		i = m.get_iter_root()
		while i:
			name_to_iter[m[i][DirModel.NAME]] = i
			i = m.iter_next(i)

		self.error = None

		files = []
		try:
			e = self.file.enumerate_children(QUERY, 0)
		except gobject.GError, ex:
			self.error = ex
		else:
			if e:
				while True:
					info = e.next_file()
					if not info:
						break
					row = make_row(info)
					if row:
						files.append(row)

		# name_to_iter contains the old contents
		# files contains the desired contents

		new = []
		for row in files:
			name = row[DirModel.NAME]

			i = name_to_iter.get(name, None)
			if i:
				m[i] = row
				del name_to_iter[name]
			else:
				new.append(row)

		# name_to_iter contains old contents that are no longer desired
		# new contains rows that are desired but were not present

		i = m.get_iter_root()
		while i:
			if m[i][DirModel.NAME] in name_to_iter:
				if not m.remove(i):
					i = None
			else:
				i = m.iter_next(i)

		for row in new:
			i = m.append(None)
			m[i] = row

dirs = weakref.WeakValueDictionary()
def get_dir_model(file):
	# Might want some caching here...
	uri = file.get_uri()
	try:
		return dirs[uri]
	except KeyError:
		dm = DirModel(file)
		dirs[uri] = dm
		return dm
