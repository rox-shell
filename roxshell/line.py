"""
@copyright: (C) 2008, Thomas Leonard
@see: U{http://roscidus.com}
"""
import os, sys, fnmatch

import _gio as gio
import gtk

import support
from directory import DirModel

class CantHandle(Exception):
	pass

class Value:
	def __init__(self, arg, value):
		self.arg = arg
		self.value = value
		self.parsed = self.parse_value(value)

	def get_entry_text(self):
		return self.value

	def get_button_label(self):
		return self.get_entry_text() or '(nothing)'
	
	def finish_edit(self):
		iv = self.arg.view.iv
		model = iv.get_model()
		if iv.flags() & gtk.HAS_FOCUS:
			cursor_path = (iv.get_cursor() or (None, None))[0]
		else:
			cursor_path = None
		selected = iv.get_selected_items()

		if cursor_path and selected and cursor_path not in selected:
			raise Warning("Cursor not in selection!")
		if cursor_path and not selected:
			selected = [cursor_path]

		self.finish_edit_with_selection(selected)
	
	def finish_edit_with_selection(self, selected):
		pass
	
	def get_default_command(self):
		return None

	def updown(self, entry, delta):
		pass

class Option(Value):
	def parse_value(self, value):
		if value.startswith('-'):
			return value
		else:
			raise CantHandle

	def expand_to_argv(self):
		return [self.value]

class Quote(Value):
	def parse_value(self, value):
		first = value[:1]
		if first and first in '"\'':
			if value[-1] == first:
				return value[1:-1]
			else:
				return value[1:]
		else:
			raise CantHandle

	def expand_to_argv(self):
		return [self.parsed]

class SelectedFiles(Value):
	abs_path = None		# GFile for the directory containing the files
	to_select = []	# Paths in the IconView's model to be selected

	def __init__(self, arg, value):
		Value.__init__(self, arg, value)
		path, leaf = support.split_expanded_path(self.parsed)
		if path:
			self.abs_path = arg.view.cwd.file.resolve_relative_path(path)
		else:
			self.abs_path = arg.view.cwd.file
		self.path = path
		self.leaf = leaf
	
	def tab(self, entry):
		value = self.parsed

		path, leaf = os.path.split(value)
		case_insensitive = (leaf == leaf.lower())
		prefix_match = None
		single_match_is_dir = False
		for i, row in self.arg.iter_matches(leaf, case_insensitive):
			name = row[DirModel.NAME]
			if prefix_match is not None:
				single_match_is_dir = False	# Multiple matches
				if not name.startswith(prefix_match):
					# Have to shorten the match then
					same = []
					for a, b in zip(prefix_match, name):
						if a == b:
							same.append(a)
						else:
							break
					prefix_match = ''.join(same)
			else:
				prefix_match = name
				if row[DirModel.INFO].get_file_type() == gio.FILE_TYPE_DIRECTORY:
					single_match_is_dir = True
		if single_match_is_dir:
			prefix_match += '/'
		if prefix_match and prefix_match != leaf:
			self.parsed = os.path.join(path, prefix_match)
			new = self.get_entry_text()
			entry.set_text(new)
			entry.set_position(len(new))
	
	def updown(self, entry, delta):
		leaf = self.leaf

		iv = self.arg.view.iv
		model = iv.get_model()
		cursor_path = self.arg.view.get_cursor_path()
		if delta > 0:
			def apply_delta(i):
				return model.iter_next(i) or model.get_iter_first()
		else:
			def apply_delta(i):
				n, = model.get_path(i)
				if n == 0:
					n = model.iter_n_children(None)
				return model.get_iter((n - 1,))
		if cursor_path:
			start = model.get_iter(cursor_path)
			i = apply_delta(start)
		else:
			start = i = model.get_iter_root()
		if not start:
			return		# Empty directory
		start = model[start][DirModel.NAME]

		case_insensitive = (leaf == leaf.lower())

		while i:
			name = model[i][DirModel.NAME]
			if name == start:
				return
			if case_insensitive:
				name = name.lower()
			if name.startswith(leaf):
				path = model.get_path(i)
				iv.unselect_all()
				iv.set_cursor(path)
				iv.select_path(path)
				return
			i = apply_delta(i)
	
class Filename(SelectedFiles):
	selected_filename = None
	selected_type = None

	def parse_value(self, value):
		return value

	def get_selected_files(self):
		leaf = self.leaf
		iv = self.arg.view.iv
		model = iv.get_model()

		self.selected_filename = None
		self.selected_type = None

		if not self.value:
			return []		# Empty entry

		if not leaf:
			self.selected_filename = ''
			self.selected_type = gio.FILE_TYPE_DIRECTORY
			return []
		elif leaf in ('.', '..'):
			self.selected_filename = leaf
			self.selected_type = gio.FILE_TYPE_DIRECTORY
			return []

		cursor_path = (self.arg.view.iv.get_cursor() or (None, None))[0]
		if cursor_path:
			cursor_filename = model[model.get_iter(cursor_path)][0]
		else:
			cursor_filename = None

		# Rules are:
		# - Select any exact match
		# - Else, select any exact case-insensitive match
		# - Else, select the cursor item if the prefix matches
		# - Else, select the first prefix match
		# - Else, select nothing
			
		# If the user only entered lower-case letters do a case insensitive match
		model = self.arg.view.iv.get_model()

		case_insensitive = (leaf == leaf.lower())
		exact_case_match = None
		exact_match = None
		prefix_match = None
		for i, row in self.arg.iter_matches(leaf, case_insensitive):
			name = row[0]
			if name == leaf:
				exact_case_match = model.get_path(i)
				break
			if case_insensitive:
				name = name.lower()
				if name == leaf:
					exact_match = model.get_path(i)
			if not prefix_match:
				prefix_match = model.get_path(i)
		if case_insensitive and cursor_filename:
			cursor_filename = cursor_filename.lower()
		if exact_case_match:
			to_select = exact_case_match
		elif exact_match:
			to_select = exact_match
		elif cursor_filename and cursor_filename.startswith(leaf):
			to_select = cursor_path
		elif prefix_match:
			to_select = prefix_match
		else:
			to_select = None

		if to_select is None:
			return []

		row = model[model.get_iter(to_select)]
		self.selected_filename = row[DirModel.NAME]
		self.selected_type = row[DirModel.INFO].get_file_type()

		return [to_select]

	def get_entry_text(self):
		return self.parsed

	def get_button_label(self):
		return self.selected_filename or '(none)'

	def expand_to_argv(self):
		if not self.value:
			raise Warning("Empty argument")
		if self.selected_filename is None:
			raise Warning("No filename selected")
		abs_path = self.abs_path.resolve_relative_path(self.selected_filename)
		return [self.arg.view.cwd.file.get_relative_path(abs_path) or abs_path.get_path()]

	def finish_edit_with_selection(self, selected):
		if not self.value:
			model = self.arg.view.iv.get_model()
			if not selected:
				raise Warning("No selection and no cursor item!")
			if len(selected) > 1:
				raise Warning("Multiple selection!")
			path = selected[0]

			row = model[model.get_iter(path)]
			self.selected_filename = row[DirModel.NAME]
			self.selected_type = row[DirModel.INFO].get_file_type()
			self.value = self.parsed = self.selected_filename

	def get_default_command(self):
		if self.selected_filename is None:
			return None
		if self.selected_type == gio.FILE_TYPE_DIRECTORY:
			return 'cd'
		else:
			return 'gvim'

class Newfile(SelectedFiles):
	def parse_value(self, value):
		if value.startswith('!'):
			return value[1:]
		raise CantHandle
	
	def get_entry_text(self):
		return '!' + self.parsed

	def get_selected_files(self):
		# No completion for new files
		# TODO: highlight if it exists
		return []

	def expand_to_argv(self):
		value = self.parsed
		path, leaf = os.path.split(value)
		if not leaf:
			raise Warning("No name given for new file!")
		if path:
			final_dir = self.arg.view.cwd.file.resolve_relative_path(path)
			unix_path = final_dir.get_path()
			if not os.path.exists(unix_path):
				os.makedirs(unix_path)
		return [value]

	def get_default_command(self):
		return 'mkdir'

class Glob(SelectedFiles):
	def updown(self, entry, delta):
		pass

	def parse_value(self, value):
		for x in value:
			if x in '*?[':
				return value
		raise CantHandle

	def get_selected_files(self):
		to_select = []
		pattern = self.leaf
		def match(m, path, iter):
			name = m[iter][0]
			if fnmatch.fnmatch(name, pattern):
				to_select.append(path)
		self.arg.view.iv.get_model().foreach(match)
		return to_select

	def expand_to_argv(self):
		pattern = self.parsed
		matches = [row[0] for i, row in self.arg.view.iter_contents() if fnmatch.fnmatch(row[0], pattern)]
		if not matches:
			raise Warning("Nothing matches '%s'!" % pattern)
		return matches

	def tab(self, entry):
		return

class BaseArgument:
	def __init__(self, view):
		self.view = view

	def get_entry_text(self):
		return ''

	def get_button_label(self):
		return '?'

	def entry_changed(self, entry):
		return

	def finish_edit(self):
		pass

	def validate(self):
		pass
	
	def tab(self, entry):
		pass

	def updown(self, entry, delta):
		pass
	
class Argument(BaseArgument):
	"""Represents a word entered by the user for a command."""
	# This is a bit complicated. An argument can be any of these:
	# - A glob pattern matching multiple filenames 	(*.html)
	# - A single filename				(index.html)
	# - A set of files selected manually		(a.html, b.html)
	# - A quoted string				('*.html')
	# - An option					(--index)
	def __init__(self, view):
		BaseArgument.__init__(self, view)
		self.result = self.parse_value('')
	
	def parse_value(self, value):
		for t in [Newfile, Quote, Option, Glob, Filename]:
			try:
				return t(self, value)
			except CantHandle:
				continue
		assert False
	
	def iter_matches(self, match, case_insensitive):
		"""Return all rows with a name matching match"""
		for i, row in self.view.iter_contents():
			name = row[0]
			if case_insensitive:
				name = name.lower()
			if name.startswith(match):
				yield i, row

	def entry_changed(self, entry):
		self.result = self.parse_value(entry.get_text())

		if not isinstance(self.result, SelectedFiles):
			self.view.iv.unselect_all()
			return

		cursor_path = (self.view.iv.get_cursor() or (None, None))[0]

		# Check which directory the view should be displaying...
		viewed = self.view.view_dir.file

		# Switch if necessary...
		if self.result.abs_path.get_uri() != viewed.get_uri():
			self.view.set_view_dir(self.result.abs_path)

		to_select = self.result.get_selected_files()

		iv = self.view.iv
		iv.unselect_all()
		if to_select:
			for path in to_select:
				iv.select_path(path)
			if cursor_path not in to_select:
				iv.set_cursor(to_select[0])

	def tab(self, entry):
		self.result.tab(entry)
	
	def get_default_command(self):
		return self.result.get_default_command()

	def updown(self, entry, delta):
		self.result.updown(entry, delta)
	
	def finish_edit(self):
		self.result.finish_edit()
	
	def get_entry_text(self):
		return self.result.get_entry_text()

	def get_button_label(self):
		return self.result.get_button_label()

	def expand_to_argv(self):
		return self.result.expand_to_argv()

class CommandArgument(BaseArgument):
	command = ''
	default_command = None

	def entry_changed(self, entry):
		self.command = entry.get_text() or None

	def get_button_label(self):
		return self.command if self.command else \
		       self.default_command if self.default_command else \
		       "Open"
	
	def expand_to_argv(self):
		return [self.command or self.default_command]

	def set_default_command_from_args(self, args):
		self.default_command = None

		if len(args) != 1:
			return

		self.default_command = args[0].get_default_command()

class ArgvView:
	def __init__(self, hbox):
		self.hbox = hbox
		self.args = []
		self.widgets = []

	def set_args(self, args):
		self.args = args
		self.edit_arg = self.args[-1]
		self.build()

	def build(self):
		for w in self.widgets:
			w.destroy()
		self.widgets = []
		self.active_entry = None

		cmd_arg = self.args[0]

		for x in self.args:
			if x is self.edit_arg:
				arg = gtk.Entry()
				arg.set_text(x.get_entry_text())
				def entry_changed(entry, x = x):
					x.entry_changed(entry)
					if x is not cmd_arg and not cmd_arg.command:
						cmd_arg.set_default_command_from_args(self.args[1:])
						self.widgets[0].set_label(cmd_arg.get_button_label())
				arg.connect('changed', entry_changed)
				self.active_entry = arg
			else:
				arg = gtk.Button(x.get_button_label())
				arg.set_relief(gtk.RELIEF_NONE)
				arg.connect('clicked', lambda b, x = x: self.activate(x))
			arg.show()
			self.hbox.pack_start(arg, False, True, 0)
			self.widgets.append(arg)
	
	def activate(self, x):
		"""Start editing argument 'x'"""
		if x is self.edit_arg:
			return
		try:
			self.edit_arg.finish_edit()
		except Warning, ex:
			self.edit_arg.view.warning(str(ex))
		self.edit_arg = x
		self.build()
		i = self.args.index(x)
		self.widgets[i].grab_focus()
	
	def space(self):
		if not self.active_entry.flags() & gtk.HAS_FOCUS:
			return False		# Not focussed
		if self.active_entry.get_position() != len(self.active_entry.get_text()):
			return False		# Not at end
		i = self.args.index(self.edit_arg)
		try:
			self.edit_arg.finish_edit()
		except Warning, ex:
			self.edit_arg.view.warning(str(ex))
		self.edit_arg = Argument(self.edit_arg.view)
		self.args.insert(i + 1, self.edit_arg)
		self.build()
		self.widgets[i + 1].grab_focus()
		return True
	
	def tab(self):
		if self.active_entry.get_position() == len(self.active_entry.get_text()):
			self.edit_arg.tab(self.active_entry)
		return True
	
	def updown(self, delta):
		if self.active_entry and self.active_entry.flags() & gtk.HAS_FOCUS:
			self.edit_arg.updown(self.active_entry, delta)
			return True
		else:
			return False
	
	def key_press_event(self, kev):
		if not self.active_entry:
			return False
		old_text = self.active_entry.get_text()
		self.active_entry.grab_focus()		# Otherwise it selects the added text
		self.active_entry.event(kev)
		return self.active_entry.get_text() != old_text
	
	def finish_edit(self):
		self.edit_arg.finish_edit()
